'use strict';

/** 
 * Создаем родительский класс
 */

class Form {
    makeAlertAfterSubmit = () => {
        alert('Форма успешно отправлена!')
    }
}

/**
 * Класс валидации формы отзывов
 */
class AddReviewForm extends Form {

    constructor(mainForm, mainFormName, mainFormRaiting, mainFormTextArea, raitingError,
        nameError1, nameError2, textArea) {
        super();

        this.mainForm = document.querySelector('.add-review-form');
        this.mainFormName = document.querySelector('.add-review-form__name');
        this.mainFormRaiting = document.querySelector('.add-review-form__raiting'); 
        this.mainFormTextArea = document.querySelector('.add-review-form__review');  
        this.raitingError = document.querySelector('.add-review-form__raiting_error');
        this.nameError1 = document.querySelector('.add-review-form__name_error1');
        this.nameError2 = document.querySelector('.add-review-form__name_error2');
        this.textArea = document.querySelector('.add-review-form__review');
        
/**
 * Проверка на наличие значение в localStorage
 */
        if (localStorage.getItem('nameInput')) {
            this.mainFormName.value = localStorage.getItem('nameInput')
        }
        if (localStorage.getItem('raitingInput')) {
            this.mainFormRaiting.value = localStorage.getItem('raitingInput')
        }

/**
 * Запись в localStorage
 */
        
        this.mainFormName.addEventListener('input', (e) => {
            localStorage.setItem('nameInput', e.target.value)
        })
        this.mainFormRaiting.addEventListener('input', (e) => {
            localStorage.setItem('raitingInput', e.target.value)
        });

        // this.mainForm.addEventListener('input', function(e) {
        // this.raitingError.style.display = 'none';
        // this.nameError1.style.display = 'none';
        // this.nameError2.style.display = 'none';
        // this.textArea.style.marginTop = '30px';
        // e.preventDefault();
        // })
    }
    
    validateName = (mainFormName) => {
        this.mainFormName.addEventListener('submit', function(e) {
            if (!this.mainFormName.value) {
                this.nameError1.style.display = 'block';
                this.textArea.style.marginTop = '0px';
                e.preventDefault();
        }
        }) 
    }

    
}
// console.log(mainFormName);
// console.log(mainFormName);
const addReviewForm = new AddReviewForm('.add-review-form');
// addReviewForm.submitForm();














