"use strict";

// exercise1

let a = '$100';
let b = '300$';
let summ = +a.slice(1) + +b.slice(0,3);
console.log(summ);

// exercise2

let message = '   привет, медвед      ';
message = message.trim();
message = message[0].toUpperCase() + message.slice(1);
console.log(message);

// exercise3

let age = prompt ('Сколько вам лет?');

if (age < 4) {
    alert(`Вам ${age} лет и вы младенец`)
}  else if (age < 12) {
    alert(`Вам ${age} лет и вы ребенок`)
}  else if (age < 19) {
    alert(`Вам ${age} лет и вы подросток`)
}  else if (age < 41) {
    alert(`Вам ${age} лет и вы познаёте жизнь`)
}  else if (age < 81) {
    alert(`Вам ${age} лет и вы познали жизнь`)
}  else {
    alert(`Вам ${age} лет и вы долгожитель`)
};

// exercise4

let str = 'Я работаю со строками как профессионал!';
str = str.split(' ');
let count = str.length;
console.log(count);