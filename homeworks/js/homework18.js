"use strict";

// exercise1

let a = '100px';
let b = '323px';

let result = parseInt(a) + parseInt(b);
console.log(result);


// exercise2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

// exercise3

let d = 0.111;
console.log(Math.ceil(d));

let e = 45.333333;
console.log(+e.toFixed(1));

let f = 3;
console.log(f**5);

let g = 400000000000000;
console.log(g);
console.log(4e14);

let h = '1';
console.log(h == 1);

// exercise4

console.log(0.1 + 0.2 === 0.3);
console.log(0.1 + 0.2);
//Сумма 0.1 + 0.2 будет равна 0.30000000000000004. Так как в JS есть потеря точности,
//связанная с хранением числа в двоичной системе. Решение может быть следующее:

let k = 0.1;
let m = 0.2;

let sum = k + m;
console.log(+sum.toFixed(1));

