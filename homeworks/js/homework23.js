'use strict'

// Exercise 1

let n = +prompt('Введите ваше число');

if (isNaN(n) || n <= 0) {
    console.log('Ошибка! Введено не число или недопустимое значение!')
} else {
    let intervalId = setInterval(function() {
        n = n - 1;
        console.log(`Осталось ${n}`);
        if (n === 0) {
            clearInterval(intervalId);
            console.log('Время вышло!')
        }
    }, 1000)
}


// Exercise 2

let promise = fetch('https://reqres.in/api/users');

promise
    .then(function(response) {
        return response.json();
    })

    .then (function(response) {
        console.log(`Получили пользователей: ${response.data.length}`);

        let users = response.data;

        users.forEach(function(user) {
            console.log(`- ${user.first_name} ${user.last_name} (${user.email})`)
        })
    })

    .catch (function() {
        console.log('Error! Database is not responding')
    })





