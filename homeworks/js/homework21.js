'use strict'

// Exercise 1

/**
 * Тестовый объект
 */
let obj1 = {}
console.log(obj1);

/**
 * Добавляем свойство тестовому объекту
 */
obj1.name = 'name';
console.log(obj1);

/**
 * Проверка объекта на наличие свойств
 * @param {object} obj - Проверяемый объект
 * @returns {boolean} - Возврат true если у объекта нет свойств, false - если свойства есть
 */

function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }
    return true;
}

/**
 * Присваивание переменной checkObj значение функции isEmpty
 */
let checkObj = isEmpty(obj1);
alert(checkObj);


// Exercise 3

/**
 * Исходный объект
 */
let salaries = {
    John:100000,
    Ann:160000,
    Pete:130000,
}

console.log(salaries);

/**
 * Начальное значение суммы зарплат
 */
let sum = 0;

/**
 * Создание нового объекта с новыми значениями свойств
 * @param {number} percent - процент, на который изменится зарплата
 * @returns {object} новый объект
 */
function raiseSalary(percent) { 
    let newSalaries = {};
    for (let key in salaries) {
        newSalaries[key] = salaries[key] + salaries[key] * percent / 100;
        sum = sum + newSalaries[key];
    }
    return newSalaries;
}

/**
 * Вывод в консоль - проверка полученных результатов
 */
console.log(raiseSalary(10));
console.log(sum);


