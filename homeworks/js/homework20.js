"use strict";

// Exercise 1

for (let i = 2; i <= 20; i = i + 2) {
    console.log(i);
}

// Exercise 2

let sum = 0;
let i = 0;

while (i < 3) {
    let value = +prompt('Введите ваше число', '');
    i++;
    if (!value)  {        
        alert('Ошибка, вы ввели не число');
        break;
    } 
    
    sum = sum + value;
}

alert('Сумма чисел равна: ' + sum);

// Exercise 3

function getNameOfMonth(month) {
    if (month === 0) {
        return('Январь');
    }  
    else if (month === 1) {
        return('Февраль');
    }
    else if (month === 2) {
        return('Март');
    }
    else if (month === 3) {
        return('Апрель');
    }
    else if (month === 4) {
        return('Май');
    }
    else if (month === 5) {
        return('Июнь');
    }
    else if (month === 6) {
        return('Июль');
    }
    else if (month === 7) {
        return('Август');
    }
    else if (month === 8) {
        return('Сентябрь');
    }
    else if (month === 9) {
        return('Октябрь');
    }
    else if (month === 10) {
        return('Ноябрь');
    }
    else if (month === 11) {
        return('Декабрь');
    }
    else return('Некорректный номер месяца')
}

let monthInRussian = getNameOfMonth(6);
console.log(monthInRussian);

for (let i = 0; i < 12; i++) {
    if (i === 9) continue;
    console.log(getNameOfMonth(i));
}

// Exercise 4

// IIFE (Immediately Invoked Function Expression) — это функция, 
// которая выполняется сразу же после того, как была определена.
// Применяется для создания изолированной области видимости, 
// например для избежания конфликтов в перезаписи переменных.