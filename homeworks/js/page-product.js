'use strict';

/**
 * Находим форму по атрибуту name из HTML
 */

let mainForm = document.forms.main;

/**
 * Находим два интересующих нас поля формы и присваиваем их в переменные
 */
let mainFormName = mainForm.name;
let mainFormRaiting = mainForm.raiting; 
let mainFormTextArea = mainForm.review; 


/** 
 * Событие submit
 */

let raitingError = document.querySelector('.add-review-form__raiting_error')
let nameError1 = document.querySelector('.add-review-form__name_error1')
let nameError2 = document.querySelector('.add-review-form__name_error2')
let textArea = document.querySelector('.add-review-form__review')


mainForm.addEventListener('submit', function(e) {
    if (!mainFormName.value) {        
        nameError1.style.display = 'block';
        textArea.style.marginTop = '0px';
        e.preventDefault();
    } else if (mainFormName.value.length < 2) {
        nameError2.style.display = 'block';
        textArea.style.marginTop = '0px';
        e.preventDefault();
    } else if (!mainFormRaiting.value || mainFormRaiting.value < 1 || mainFormRaiting.value > 5) {
        raitingError.style.display = 'block';
        textArea.style.marginTop = '0px';
        e.preventDefault();
    } else {
        alert('Спасибо за ваш отзыв!')
    }
})

/**
 * Событие input
 */

mainForm.addEventListener('input', function(e) {
    raitingError.style.display = 'none';
    nameError1.style.display = 'none';
    nameError2.style.display = 'none';
    textArea.style.marginTop = '30px';
    e.preventDefault();
})


// Homework-25

mainForm.addEventListener('input', function(e) {
    let nameInput = mainFormName.value;
    localStorage.setItem('nameInput', nameInput)

    let raitingInput = mainFormRaiting.value;
    localStorage.setItem('raitingInput', raitingInput)

    let textAreaInput = textArea.value;
    localStorage.setItem('textAreaInput', textAreaInput)
})

mainFormName.value = localStorage.getItem('nameInput');
mainFormRaiting.value = localStorage.getItem('raitingInput');
mainFormTextArea.value = localStorage.getItem('textAreaInput');

mainForm.addEventListener('submit', function(e) {
    localStorage.clear();
});


// Промежуточная аттестация

let goodsCountInCart = localStorage.getItem('goodsCountInCart') ? Number(localStorage.getItem('goodsCountInCart')) : 0;
let cartCount = document.querySelector('.cart__count');
let cartCounter = document.querySelector('.cart__counter');
let orderBtn = document.querySelector('.order__btn');


if(goodsCountInCart === 1) {
    cartCounter.innerHTML = goodsCountInCart;
    cartCount.classList.toggle('cart__count_active');
    orderBtn.innerHTML = 'Товар уже в корзине';
    orderBtn.classList.add('order__btn_active')
}

orderBtn.addEventListener('click', function(e) {
    if (goodsCountInCart === 1) {
        goodsCountInCart = 0;
        localStorage.removeItem('goodsCountInCart');
        cartCount.classList.toggle('cart__count_active');
        orderBtn.classList.remove('order__btn_active')
        orderBtn.innerHTML = 'Добавить в корзину'
    } else {
        goodsCountInCart = 1;
        localStorage.setItem('goodsCountInCart', goodsCountInCart);
        cartCounter.innerHTML = goodsCountInCart;
        cartCount.classList.toggle('cart__count_active');
        orderBtn.classList.add('order__btn_active');
        orderBtn.innerHTML = 'Товар уже в корзине';
    }
})
















