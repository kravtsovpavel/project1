'use strict'

// Exercise 1

/**
 * Массив для проверки работы функции
 */
let arr = [5, 7, 200, -100, true, 8, 50, 'www', 30, 'qqq', 100];

function getSumm(arr) {
    let sum = 0;
        for (let i = 0; i < arr.length; i++) {
            if (typeof(arr[i]) != "number") {
                continue
            } else {
                sum = sum + arr[i];
            }            
        }
    return sum;
}

let result = getSumm(arr);
console.log(result);

// Exercise 3

let cart = [];
console.log(cart);

cart = new Set(cart);

function addToCart(id) {
    cart.add(id);
    return cart
}

function removeFromCart(id) {
    cart.delete(id);
    return cart
}

/** Проверка работы функций в консоли  */

console.log(addToCart(1234));
console.log(addToCart(3456));
console.log(addToCart(3456));
console.log(addToCart(5678));
console.log(removeFromCart(1234));
console.log(removeFromCart(3456));
console.log(removeFromCart(3456));
console.log(removeFromCart(5678));








